/* Load the express.js module into our application and saved it in a variable called express */

const express = require("express");
/*
	This creates an application that uses express and store it as app
	- app is our server
*/
const app = express();

const port = 4000;

// middleware
	// simplified parsing of JSON
app.use(express.json())

// mock data
let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "dontTalkAboutMe"
	},
	{
		username: "Luisa",
		email: "stronggirl@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 259,
		isActive: true
	}
]

// app.get(<endpoint>, <function for req and res>)
app.get('/', (req, res) => {
	// response
	res.send('Hello from my first expressJS API')
	// OR res.status(200).send('Hello from my first expressJS API')
});

/*
Mini Activity: 5 mins

    >> Create a get route in Expressjs which will be able to send a message in the client:

        >> endpoint: /greeting
        >> message: 'Hello from Batch182-surname'

    >> Test in postman
    >> Send your response in hangouts
*/
app.get('/greetings', (req, res) => {
	res.send('Hello from Batch182-Gabutin')
});

// retrieval of the mock database
app.get('/users', (req, res) => {
	// send method stringifies data automatically
	// sends 'users' data stored under 'mock data'
	res.send(users);
	// alternative: res.json(users); -same response
});

// POST Method: user can input new data in API, which fills in the 'newUser' object
app.post('/users', (req, res) => {
	console.log(req.body); // result: {} - if no input
	// assigns properties to input from user in API
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}
	// pushes newUser into users database
	users.push(newUser);
	console.log(users); // shows users in terminal

	// sends the updated users object back in the API
	res.send(users);
});

// DELETE Method:
app.delete('/users', (req, res) => {
	users.pop(); // deletes last
	console.log(users);

	// sends the updated users object back in the API
	res.send(users)
});

// PUT Method:
	// goal: update user's password
	// ':index' - wildcard, can be replaced with object _id or index number
	// url: localhost:4000/users/0
app.put('/users/:index', (req, res) => {

	console.log(req.body);
	// an object that contains the value of URL params
	console.log(req.params);

	// converts acquired index into number (parseInt)
	// ['0'] turns into [0]
	let index = parseInt(req.params.index);
	// users[0].password
	users[index].password = req.body.password;

	res.send(users[index]);
});

/*
Mini-Activity: 5 mins

    >> endpoint: users/:index
    >> method: PUT

        >> Update a user's username
        >> Specify the user using the index in the params
        >> Put the updated username in request body
        >> Send the updated user as a response
        >> Test in Postman 
        >> Send your screenshots in Hangouts
*/
app.put('/users/update/:index', (req, res) => {

	let index = parseInt(req.params.index);
	
	users[index].username = req.body.username;

	res.send(users[index]);
});

// Retrieval of single user
app.get('/users/getSingleUser/:index', (req, res) => {
	console.log(req.params) // result: {index: '1'}
	let index = parseInt(req.params.index); // parseInt('1')
	console.log(index); // result: index converted to number
	res.send(users[index]);
});

// A C T I V I T Y S O L U T I O N s33

app.get('/items', (req, res) => {
	res.send(items);
});

app.post('/items', (req, res) => {
	let newItem = {
			name: req.body.name,
			price: req.body.price,
			isActive: req.body.isActive
		}
		items.push(newItem);
		res.send(items);
});

app.put('/items/:index', (req, res) => {
	let index = parseInt(req.params.index);
	items[index].price = req.body.price;
	res.send(items[index]);
});

// A C T I V I T Y S O L U T I O N s34
/*
Activity #2
>> Create a new route with '/items/getSingleItem/:index' endpoint. 
    >> This route should allow us to GET the details of a single item.
        -Pass the index number of the item you want to get via the url as url params in your postman.
        -http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
    
    >> Send the request and back to your api
        -get the data from the url params.
        -send the particular item from our array using its index to the client.

>> Create a new route to update an item with '/items/archive/:index' endpoint.    (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to de-activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
        -send the updated item in the client

>> Create a new route to update an item with '/items/activate/:index' endpoint. (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
        -send the updated item in the client

>> Create a new collection in Postman called s34-activity
>> Save the Postman collection in your s33-s34 folder
*/

app.get('/items/getSingleItem/:index', (req, res) => {
	let index = parseInt(req.params.index);
	res.send(items[index]);
});

app.put('/items/archive/:index', (req, res) => {
	let index = parseInt(req.params.index);
	items[index].isActive = false;
	res.send(items[index]);
});

app.put('/items/activate/:index', (req, res) => {
	let index = parseInt(req.params.index);
	items[index].isActive = true;
	res.send(items[index]);
});

// port
app.listen(port, () => console.log(`Server is running at port ${port}`))